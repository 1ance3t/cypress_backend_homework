Cypress.Commands.add('fillField', (num, field) => {
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (let i = 0; i < num; i++) {
        cy.get(`${field}`)
            .type(possible.charAt(Math.floor(Math.random() * possible.length)))
    }
})