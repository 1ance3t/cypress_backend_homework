class task1 {
    openTask1() {
        cy.visit(`${Cypress.env('task1Site')}`);
    }
    nameField() {
       return cy
            .get('[data-cy=nameField]')
    }
    emailField() {
        return cy
            .get('[data-cy=emailField]')
    }
    messageField() {
        return cy
            .get('[data-cy=messageTextarea]')
    }
    sendButton() {
        return cy
        .get('[data-cy=sendButton]')
    }
    errorMessage(){
        return cy
            .get('[data-cy=errorMessage]', {timeout: 3000})
    }
    errorAlert(){
        return cy
            .get('[data-cy=errorAlert]')
    }

    successAlert(){
        return cy
            .get('[data-cy=successAlert]')
    }

    sparkpostButton(){
        return cy
            .get('[data-cy=sparkpost]')
    }
    
    
}

export default new task1()