/// <reference types="Cypress" />
import task1 from '../page_object/task1'

context('Task 1. Email sending', () => {

    describe('General checks', () => {

        it('Checking the titile and noindex nofollow', () => {
            task1.openTask1()

            cy.title()
                .should('eq', 'Task 1')

            cy.get('head meta[name="robots"]')
                .should("have.attr", "content", "noindex, nofollow");
        })

        it('Checking the robots.txt ', () => {
            task1.openTask1()
            cy.url().then(url => {
                cy.request(`${url}/robots.txt`).then(resp => {
                    expect(resp.body).to.contain("User-agent: *\nDisallow: /");
                    
                })
            })
        })

    })

    describe('Testing the name field', () => {

        it('Negative: Trying send email without name field (SMTP)', () => {
            task1.openTask1()

            task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Name field is required')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with one symbol in the name field (SMTP)', () => {
            task1.openTask1()
        
            task1.nameField()
                .type('a')
        
            task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)
        
            task1.messageField()
                .type('12345')
        
            task1.sendButton()
                .click()
        
            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Name must be longer than or equal to 2 characters')
        
            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')
        
        })

        it('Negative: Trying send email with more than 60 symbols in the name field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .as('name')

            cy.fillField(62, '@name')

            task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Name must be shorter than or equal to 60 characters')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with incorrect symbols in the name field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type(' /^[a-z]+$/si')



            task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Name should only consist of the letters')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Checking cropp spaces in the first part of name (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('         name')

            task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)

            task1.messageField()
                .type('12345')

            task1.nameField()
                .should('have.value', 'name')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with 2 symbols in the name field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('aa')

            task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

    })

    describe('Testing the email field', () => {

        it('Negative: Trying send email without email field (SMTP)', () => {
            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email field is required')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with more than 254 symbols in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@gmail.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email must be shorter than or equal to 254 characters')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with one symbol in email field (SMTP)', () => {
            task1.openTask1()
        
            task1.nameField()
                .type('Tyler Durden')
        
            task1.emailField()
                .type('a')
        
            task1.messageField()
                .type('12345')
            
            task1.sendButton()
                .click()
        
            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email must be longer than or equal to 5 characters')
        
            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')
        
        })
        
        it('Negative: Trying send email with no @ or domain in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('plainaddress')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with missing @ in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email.domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with missing address in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('@domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with garbage in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('#@%^%#$@#$@#.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with copy/paste from address book with name in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('Joe Smith <email@domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with superfluous text in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email@domain.com (Joe Smith)')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with two @ in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email@domain@domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with leading dot in addres in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('.email@domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with trailing dot in address in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email.@domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with multiple dots in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email..email@domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with leading dot in domain in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email@.domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with invalid IP format in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email@111.222.333.44444')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with multiple dots in the domain in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email@domain..com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })
        
        it('Negative: Trying send email with leading dash in domain in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email@-domain.com')

            task1.messageField()
                .type('12345')
            
            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Email should have format RFC')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Positive: Trying send email with 254 symbols in the email field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@gmail.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')


        })

        it('Positive: Checking cropp spaces in the first part of name (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('     example@gmail.com')

            task1.messageField()
                .type('12345')

            task1.emailField()
                .should('have.value', 'example@gmail.com')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')


        })

        it('Positive: Trying send email with valid email format (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('example@gmail.com')

            task1.messageField()
                .type('12345')

            task1.emailField()
                .should('have.value', 'example@gmail.com')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')


        })

        it('Positive: Trying send email with dot in the address (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('firstname.lastname@domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with subdomain (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('email@subdomain.domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with plus in address (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type('firstname+lastname@domain.com')

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with unnecessary quotes around address (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type(`\"email\"@domain.com`)

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with ucessary quotes around address (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type(`\"email..email\"@domain.com`)

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with numeric address (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type("1234567890@domain.com")

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with correct data (Sparkpost)', () => {
            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)

            task1.messageField()
                .type('12345')

            task1.sparkpostButton()
                .click()
            
            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with dash in domain (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type("email@domain-one.com")

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with numeric domain (SMTP)', () => {

            task1.openTask1()
        
            task1.nameField()
                .type('Tyler Durden')
        
            task1.emailField()
                .type('email@123.123.123.123')
        
            task1.messageField()
                .type('12345')
        
            task1.sendButton()
                .click()
        
            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')
        
        })

        it('Positive: Trying send email with underscore (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type("_______@domain.com")

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with >3 char TLD (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type("email@domain.name")

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with 2 char TLD (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type("email@domain.co.jp")

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Positive: Trying send email with dash in address (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

            task1.emailField()
                .type("firstname-lastname@domain.com")

            task1.messageField()
                .type('12345')

            task1.sendButton()
                .click()

            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })
    })

    describe('Testing the message field', () => {

        it('Negative: Trying send email without message field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

             task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)

            task1.sendButton()
                .click()

            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Message field is required')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email with more then 500 symbols in message field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

             task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)

            task1.messageField()
                .as('message')

            cy.fillField(501, '@message')

            task1.sendButton()
            .click()
                
            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Message must be shorter than or equal to 500 characters')

            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')

        })

        it('Negative: Trying send email wit one symbol in message field (SMTP)', () => {

            task1.openTask1()
        
            task1.nameField()
                .type('Tyler Durden')
        
             task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)
        
            task1.messageField()
                .type('a')

            task1.sendButton()
                .click()    
        
            task1.errorMessage()
                .should('be.visible')
                .should('have.text', 'Message must be longer than or equal to 5 characters')
        
            task1.errorAlert()
                .should('be.visible')
                .contains('There are errors in the send form')
        
        })

        it('Positive: Trying send email with 5 symbols in message field (SMTP)', () => {

            task1.openTask1()

            task1.nameField()
                .type('Tyler Durden')

             task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)

            task1.messageField()
                .as('message')

            cy.fillField(5, '@message')

            task1.sendButton()
            .click()
                
            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')

        })

        it('Negative: Checking cropp spaces in the first part of message (SMTP)', () => {

            task1.openTask1()
        
            task1.messageField()
                .type('            12345')
        
            task1.nameField()
                .type('Tyler Durden')
        
            task1.emailField()
                .type(`${Cypress.env('emailForTask1')}`)
        
            task1.messageField()
                .should('have.value', '12345')
        
            task1.sendButton()
                .click()
        
            task1.successAlert()
                .should('be.visible')
                .contains('Your letter has been successfully sent for processing')
        
        })
        
    })

})
